import java.util.*;

public class Main {
    public static void main(String[] args) {

        CsvToMap obj = new CsvToMap();
        List<Map<String, String>> matchesData = obj.Converter("matches");
        List<Map<String, String>> deliveriesData = obj.Converter("deliveries");
//        numberOfMatchesPlayedPerYear(matchesData);
//        numberOfMatchesWonPerTeam(matchesData);
//        extraRunsConcededPerTeamInYear(matchesData, deliveriesData, "2016");
//        topEconomicalBowlersInYear(matchesData, deliveriesData, "2015");
//        highestNumberOfPlayerOfTheMatch(matchesData);
        mostDismissalsByAnotherPlayer(deliveriesData);

    }

    static void numberOfMatchesPlayedPerYear(List<Map<String, String>> matchesData) {
        Map<String, Integer> matchesPerYear = new HashMap<>();
        for (Map<String, String> match : matchesData) {
            String season = match.get("season");
            matchesPerYear.put(season, matchesPerYear.getOrDefault(season, 0) + 1);
        }
        System.out.println(matchesPerYear);
    }

    static void numberOfMatchesWonPerTeam(List<Map<String, String>> matchesData) {

        Map<String, Integer> matchesPerTeam = new HashMap<>();
        for (Map<String, String> match : matchesData) {
            String winner = match.get("winner");
            matchesPerTeam.put(winner, matchesPerTeam.getOrDefault(winner, 0) + 1);
        }
        for (String key : matchesPerTeam.keySet()) {
            if (!key.isEmpty()) {
                System.out.println(key + " " + matchesPerTeam.get(key));
            }
        }
    }

    static void extraRunsConcededPerTeamInYear(List<Map<String, String>> matchesData, List<Map<String, String>> deliveriesData, String year) {
        List<String> matchesIdsInYear = new ArrayList<>();
        for (Map<String, String> match : matchesData) {
            String season = match.get("season");
            String id = match.get("id");
            if (year.equals(season)) {
                matchesIdsInYear.add(id);
            }
        }
        Map<String, Integer> extraRunsByTeam = new HashMap<>();
        for (Map<String, String> delivery : deliveriesData) {
            String matchId = delivery.get("match_id");
            String bowlingTeam = delivery.get("bowling_team");
            String extraRuns = delivery.get("extra_runs");
            if (matchesIdsInYear.contains(matchId)) {
                extraRunsByTeam.put(bowlingTeam, Integer.valueOf(extraRunsByTeam.getOrDefault(bowlingTeam, Integer.valueOf(extraRuns)) + Integer.valueOf(extraRuns)));
            }
        }
        System.out.println(extraRunsByTeam);
    }

    static void topEconomicalBowlersInYear(List<Map<String, String>> matchesData, List<Map<String, String>> deliveriesData, String year) {
        List<String> matchesIdsInYear = new ArrayList<>();
        for (Map<String, String> match : matchesData) {
            String season = match.get("season");
            String id = match.get("id");
            if (year.equals(season)) {
                matchesIdsInYear.add(id);
            }
        }

        Map<String, Integer> bowlerRuns = new HashMap<>();
        Map<String, Integer> bowlerBalls = new HashMap<>();

        for (Map<String, String> delivery : deliveriesData) {
            String matchId = delivery.get("match_id");
            String bowler = delivery.get("bowler");
            Integer wideRuns = Integer.valueOf(delivery.get("wide_runs"));
            Integer penaltyRuns = Integer.valueOf(delivery.get("penalty_runs"));
            Integer batsmanRuns = Integer.valueOf(delivery.get("batsman_runs"));
            Integer extraRuns = Integer.valueOf(delivery.get("extra_runs"));
            Integer runs = wideRuns + penaltyRuns + batsmanRuns + extraRuns;

            if (matchesIdsInYear.contains(matchId)) {
                bowlerRuns.put(bowler, bowlerRuns.getOrDefault(bowler, runs) + runs);
                if (wideRuns == 0 && penaltyRuns == 0) {
                    bowlerBalls.put(bowler, bowlerBalls.getOrDefault(bowler, 0) + 1);
                }
            }
        }
        Map<String, Double> bowlersEconomy = new HashMap<>();
        for (String bowler : bowlerRuns.keySet()) {
            int runs = bowlerRuns.get(bowler);
            int balls = bowlerBalls.getOrDefault(bowler, 1);
            double overs = balls / 6.0;
            double economyRate = runs / overs;
            bowlersEconomy.put(bowler, economyRate);
        }

        List<Map.Entry<String, Double>> sortedBowlers = new ArrayList<>(bowlersEconomy.entrySet());
        sortedBowlers.sort(Map.Entry.comparingByValue());

        for (Map.Entry<String, Double> entry : sortedBowlers) {
            System.out.println("Bowler: " + entry.getKey() + ", Economy Rate: " + entry.getValue());
        }
    }

    static void highestNumberOfPlayerOfTheMatch(List<Map<String, String>> matchesData) {
        Set<String> seasons = new HashSet<>();
        for (Map<String, String> match : matchesData) {
            String season = match.get("season");
            seasons.add(season);
        }
        Map<String, Map<String, Integer>> playerOfTheMatch = new HashMap<>();
        for (String season : seasons) {
            Map<String, Integer> matchPlayer = new HashMap<>();
            for (Map<String, String> match : matchesData) {
                if (season.equals(match.get("season"))) {
                    String player = match.get("player_of_match");
                    matchPlayer.put(player, matchPlayer.getOrDefault(player, 0) + 1);
                }
            }

            List<Map.Entry<String, Integer>> sortedMatchPlayers = new ArrayList<>(matchPlayer.entrySet());
            sortedMatchPlayers.sort(Map.Entry.comparingByValue());

            Map<String, Integer> sortedMap = new LinkedHashMap<>();
            for (Map.Entry<String, Integer> entry : sortedMatchPlayers.reversed()) {
                sortedMap.put(entry.getKey(), entry.getValue());
                break;
            }
            playerOfTheMatch.put(season, sortedMap);
        }

        List<Map.Entry<String, Map<String, Integer>>> manOfThematch = new ArrayList<>(playerOfTheMatch.entrySet());
        manOfThematch.sort(Map.Entry.comparingByKey());
        System.out.println(manOfThematch);
    }

    static void mostDismissalsByAnotherPlayer(List<Map<String, String>> deliveriesData) {
        Map<String, Map<String, Integer>> mostDismissals = new HashMap<>();
        System.out.println(deliveriesData.get(11));
        for (Map<String, String> delivery : deliveriesData) {
            if (!delivery.get("player_dismissed").isEmpty()) {
                String bowler = delivery.get("bowler");
                String batsman = delivery.get("player_dismissed");
                if (!mostDismissals.containsKey(batsman)) {
                    Map<String, Integer> allBowlers = new HashMap<>();
                    allBowlers.put(bowler, 1);
                    mostDismissals.put(batsman, allBowlers);
                } else {
                    Map<String, Integer> arr = new HashMap<>(mostDismissals.get(batsman));
                    arr.put(bowler, arr.getOrDefault(bowler, 0) + 1);
                    mostDismissals.put(batsman, arr);
                }
            }
        }
        for ( Map.Entry<String, Map<String, Integer>> entry:mostDismissals.entrySet()){
            List<Map.Entry<String,Integer>> entryList=new ArrayList<>(entry.getValue().entrySet());
            entryList.sort(Map.Entry.comparingByValue());
            System.out.println(entryList.reversed());
        }
    }
}



