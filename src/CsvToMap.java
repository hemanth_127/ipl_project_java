import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CsvToMap {

    public static List<Map<String, String>> Converter(String file) {
        String csvFile = String.format("src/data/%s.csv", file);
        List<Map<String, String>> jsonObjects = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            String[] headers = parseCsvLine(br.readLine());

            while ((line = br.readLine()) != null) {
                String[] data = parseCsvLine(line);
                Map<String, String> jsonObject = new LinkedHashMap<>();

                for (int i = 0; i < headers.length; i++) {
                    jsonObject.put(headers[i], data[i]);
                }

                jsonObjects.add(jsonObject);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonObjects;
    }

    private static String[] parseCsvLine(String line) {
        List<String> result = new ArrayList<>();
        StringBuilder currentField = new StringBuilder();
        boolean inQuotes = false;
        for (char c : line.toCharArray()) {
            if (c == '"') {
                inQuotes = !inQuotes;
            } else if (c == ',' && !inQuotes) {
                result.add(currentField.toString().trim());
                currentField.setLength(0);
            } else {
                currentField.append(c);
            }
        }
        result.add(currentField.toString().trim());
        return result.toArray(new String[0]);
    }
}
